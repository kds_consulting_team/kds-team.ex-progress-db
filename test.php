<?php

declare(strict_types=1);

$dsn = 'Progress';
putenv('ODBCINI=/etc/odbc.ini');
putenv('ODBCINST=/etc/odbcinst.ini');
print 'Test ODBC Progress <br>';
$sql = 'SELECT * FROM PUB.Customer';
$conn_id = odbc_connect('Progress', 'sysprogress', 'sysprogress', SQL_CUR_USE_ODBC);
if ($conn_id) {
    echo "connected to DSN: $dsn <br>";
    $result = odbc_do($conn_id, $sql);
    if ($result) {
        echo "executing '$sql'";
        echo ' Results: ';
        odbc_result_all($result, "BGCOLOR='#AAFFAA' border=3 width=30% bordercolordark='#FF0000'");
        echo 'freeing result';
        odbc_free_result($result);
    } else {
        echo "cannot execute '$sql' ";
    }
    echo " closing connection $conn_id";
    odbc_close($conn_id);
} else {
    echo " cannot connect to DSN: $dsn ";
}
/* phpinfo(); */
