<?php

declare(strict_types=1);

namespace Keboola\DbExtractor\Extractor;

class OdbcConnection extends \Keboola\DbExtractor\Adapter\ODBC\OdbcConnection
{
    use QuoteTrait;
    use QuoteIdentifierTrait;

    public function testConnection(): void
    {
        // SELECT 1 is not working in some Informix databases
        $this->query("SELECT * FROM PUB.\"_File\" where \"_Tbl-Type\" = 'S'", 1);
    }
}
