<?php

declare(strict_types=1);

namespace Keboola\DbExtractor\Metadata;

use Keboola\DbExtractor\Extractor\OdbcConnection;
use Keboola\DbExtractor\Adapter\Metadata\MetadataProvider;
use Keboola\DbExtractor\Adapter\ODBC\OdbcNativeMetadataProvider;
use Keboola\DbExtractorConfig\Configuration\ValueObject\DatabaseConfig;

class OdbcMetadataProviderFactory
{
    private OdbcConnection $connection;

    private DatabaseConfig $dbConfig;

    public function __construct(OdbcConnection $connection, DatabaseConfig $dbConfig)
    {
        $this->connection = $connection;
        $this->dbConfig = $dbConfig;
    }

    public function create(): MetadataProvider
    {
        return new OdbcNativeMetadataProvider($this->connection);
    }
}
