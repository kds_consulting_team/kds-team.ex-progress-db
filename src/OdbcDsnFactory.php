<?php

declare(strict_types=1);

namespace Keboola\DbExtractor;

use Keboola\DbExtractor\Configuration\OdbcDatabaseConfig;

class OdbcDsnFactory
{
    public const ODBC_DRIVER_NAME = 'Progress';

    public function create(OdbcDatabaseConfig $dbConfig): string
    {
        // ServerName is additional connection parameter required by Informix
        $dsn = sprintf(
            'Driver={%s};Host=%s;Port=%s;DB=%s',
            self::ODBC_DRIVER_NAME,
            $dbConfig->getHost(),
            $dbConfig->getPort(),
            $dbConfig->getDatabase()
        );

        return $dsn;
    }
}
