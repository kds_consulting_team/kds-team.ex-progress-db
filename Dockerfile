FROM php:7.4-cli

ARG COMPOSER_FLAGS="--prefer-dist --no-interaction"
ARG DEBIAN_FRONTEND=noninteractive
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_PROCESS_TIMEOUT 3600



COPY docker/php-prod.ini /usr/local/etc/php/php.ini
COPY docker/composer-install.sh /tmp/composer-install.sh


RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        locales \
        unzip \
        ssh \
        libncurses5 \
        libicu-dev \
        unixodbc \
        mc \
        unixodbc-dev \
	&& rm -r /var/lib/apt/lists/* \
	&& sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
	&& locale-gen \
	&& chmod +x /tmp/composer-install.sh \
	&& /tmp/composer-install.sh

ENV LANGUAGE=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

# INTL
RUN docker-php-ext-configure intl \
    && docker-php-ext-install intl

# PHP ODBC
# https://github.com/docker-library/php/issues/103#issuecomment-353674490
RUN set -ex; \
    docker-php-source extract; \
    { \
        echo '# https://github.com/docker-library/php/issues/103#issuecomment-353674490'; \
        echo 'AC_DEFUN([PHP_ALWAYS_SHARED],[])dnl'; \
        echo; \
        cat /usr/src/php/ext/odbc/config.m4; \
    } > temp.m4; \
    mv temp.m4 /usr/src/php/ext/odbc/config.m4; \
    docker-php-ext-configure odbc --with-unixODBC=shared,/usr; \
    docker-php-ext-install odbc; \
    docker-php-ext-install intl; \
    docker-php-source delete
	
	
# ODBC driver
#######################################
COPY odbc/ /odbc/
COPY odbc/progress/ /odbc/
COPY odbc/install.ini /odbc/install.ini
COPY odbc/odbcinst.ini /etc/odbcinst.ini
#COPY odbc/odbc.ini /etc/odbc.ini
RUN chmod +x /odbc/proinst
RUN chmod +x /odbc/_ovrly

RUN odbc/proinst -b /odbc/install.ini

RUN cp /usr/dlc/odbc/lib/libpgicu27.so /usr/lib

# SET ENV
ENV ODBCINI=/etc/odbc.ini
ENV ODBCINST=/etc/odbcinst.ini
ENV LD_LIBRARY_PATH=/usr/dlc/odbc:/usr/dlc/odbc/lib
#######################################




WORKDIR /code/
# Fix SSL configuration to be compatible with older servers
RUN \
    # https://wiki.debian.org/ContinuousIntegration/TriagingTips/openssl-1.1.1
    sed -i 's/CipherString\s*=.*/CipherString = DEFAULT@SECLEVEL=1/g' /etc/ssl/openssl.cnf \
    # https://stackoverflow.com/questions/53058362/openssl-v1-1-1-ssl-choose-client-version-unsupported-protocol
    && sed -i 's/MinProtocol\s*=.*/MinProtocol = TLSv1/g' /etc/ssl/openssl.cnf

## Composer - deps always cached unless changed
# First copy only composer files
COPY composer.* /code/

# Download dependencies, but don't run scripts or init autoloaders as the app is missing
RUN composer install $COMPOSER_FLAGS --no-scripts --no-autoloader

# Copy rest of the app
COPY . /code/

# Run normal composer - all deps are cached already
RUN composer install $COMPOSER_FLAGS

CMD ["php", "/code/src/run.php"]

